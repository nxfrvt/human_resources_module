#!/usr/bin/env python3

__author__ = "Bartłomiej Mazurek"
__copyright__ = "Copyright (c) 2020 Bartłomiej Mazurek"
__email__ = "mazurekb02@gmail.com"
__version__ = "1.0"

import unittest
from human_resources_module.human import Human


class HumanTest(unittest.TestCase):
    def test_jan_kowalski(self):
        jan = Human("Jan", "Kowalski")
        self.assertEqual("Jan", jan.name, "Found name does not match expected!")
        self.assertEqual("Kowalski", jan.surname, "Found surname does not much expected!")

    def test_anna_nowak(self):
        anna = Human("Anna", "Nowak")
        self.assertEqual("Anna", anna.name, "Found name does not match expected!")
        self.assertEqual("Nowak", anna.surname, "Found surname does not much expected!")


if __name__ == "__main__":
    unittest.main()
