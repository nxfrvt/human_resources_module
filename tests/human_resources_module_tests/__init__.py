#!/usr/bin/env python3

__author__ = "Bartłomiej Mazurek"
__copyright__ = "Copyright (c) 2020 Bartłomiej Mazurek"
__email__ ="mazurekb02@gmail.com"
__version__ = "1.0"


from tests.human_resources_module_tests.human_tests import HumanTest
import unittest

if __name__ == "__main__":
    unittest.main()

