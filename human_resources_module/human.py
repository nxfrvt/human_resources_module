#!/usr/bin/env python3

__author__ = "Bartłomiej Mazurek"
__copyright__ = "Copyright (c) 2020 Bartłomiej Mazurek"
__email__ = "mazurekb02@gmail.com"
__version__ = "1.0"


class Human:
    """This is Human class"""
    def __init__(self, name, surname):
        """ Ctor
        :param name: First name
        :param surname: Last name
        """
        self._name = name
        self._surname = surname

    @property
    def name(self):
        return self._name

    @property
    def surname(self):
        return self._surname

