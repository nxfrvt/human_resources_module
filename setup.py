import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="human_resources_module",
    version="1.0",
    author="Bartłomiej Mazurek",
    author_email="n3frytpl@gmail.com",
    description="Module for human resources applications",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/nxfrvt/human_resources_module",
    packages=setuptools.find_packages(),
    classifiers=[
        "Development Status :: 1 - Planning",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)