# Human Resources Module [![pipeline status](https://gitlab.com/nxfrvt/human_resources_module/badges/master/pipeline.svg)](https://gitlab.com/nxfrvt/human_resources_module/-/commits/master) [![coverage report](https://gitlab.com/nxfrvt/human_resources_module/badges/master/coverage.svg)](https://gitlab.com/nxfrvt/human_resources_module/-/commits/master)

## Table of contents
* [General info](#general-info)
* [Authors](#authors)
* [License](#license)

## General info

Human resources module

## Authors
* Bartłomiej Mazurek 


See also the list of [contributors](https://gitlab.com/nxfrvt/human_resources_module/-/graphs/master) who participated in this project.

## License
[MIT](https://choosealicense.com/licenses/mit/)

